With FoundCAT, which is a server based Django application, you can learn languages by translating texts together with other users collaboratively on the web.

FoundCAT was started at the FH Aachen under the leadership of Prof. Dr. rer. nat. Alexander Ferrein, our team's mastermind Gary Evans, Dipl.-Des. Winfried Kock (university didactics) and was developed by the students Sebastian Utzerath, Dominik Schür, and Alexander Kern.

FoundCAT is released under the GNU AGPL v3+ and is mainly based on Weblate by Michal Čihař.

Please contact us before working on the project since there are some known issues. Also we are really interested in seeing who else is interested in development and would be happy to work together. A good place to talk is our public forum at https://bluecloud.academy/mod/forum/view.php?f=5

On that server there's also a public instance of FoundCAT if you want to try it out. Everyone is welcome to join, the more users the better.

If you want to see what we want to work on next, see our priority list at https://gitlab.com/FoundCAT/FoundCAT/wikis/Old_roadmap

Regards,

Team FoundCAT
