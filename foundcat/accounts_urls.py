# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Original author: Michal Čihař <michal@cihar.com> part of Weblate
# <https://weblate.org/> license: GPLv3+

from django.conf.urls import url, include

import foundcat.logout_views
import social_django.views
import foundcat.views.accounts
import weblate.accounts.views

# Follows copy of social_django.urls with few changes:
# - authentication requires POST (issue submitted upstream)
# - authentication stores current user (to avoid CSRF on complete)
# - removed some configurability (just to avoid additional deps)
# - the association_id has to be numeric (patch accepted upstream)
social_urls = [
    # authentication / association
    url(
        r'^login/(?P<backend>[^/]+)/$',
        weblate.accounts.views.social_auth,
        name='begin'
    ),
    url(
        r'^complete/(?P<backend>[^/]+)/$',
        weblate.accounts.views.social_complete,
        name='complete'
    ),
    # disconnection
    url(
        r'^disconnect/(?P<backend>[^/]+)/$',
        social_django.views.disconnect,
        name='disconnect'
    ),
    url(
        r'^disconnect/(?P<backend>[^/]+)/(?P<association_id>\d+)/$',
        social_django.views.disconnect,
        name='disconnect_individual'
    ),
]


urlpatterns = [
    url(
        r'^email-sent/$',
        weblate.accounts.views.EmailSentView.as_view(
            template_name='accounts/email-sent.html',
        ),
        name='email-sent'
    ),
    url(r'^password/', weblate.accounts.views.password, name='password'),
    url(
        r'^reset-api-key/',
        weblate.accounts.views.reset_api_key,
        name='reset-api-key'
    ),
    url(
        r'^reset/', weblate.accounts.views.reset_password,
        name='password_reset'
    ),
    url(r'^logout/', foundcat.logout_views.foundcat_logout, name='logout'),
#    url(r'^log_out/', foundcat.logout_views.foundcat_logout, name='logout_foundcat'),
    url(r'^profile/', foundcat.views.accounts.settings, name='profile'),
    url(
        r'^watch/(?P<project>[^/]+)/',
        weblate.accounts.views.watch,
        name='watch'
    ),
    url(
        r'^unwatch/(?P<project>[^/]+)/',
        weblate.accounts.views.unwatch,
        name='unwatch'
    ),
    url(r'^remove/', weblate.accounts.views.user_remove, name='remove'),
    url(r'^confirm/', weblate.accounts.views.confirm, name='confirm'),
    url(r'^login/$', weblate.accounts.views.weblate_login, name='login'),
    url(r'^register/$', weblate.accounts.views.register, name='register'),
    url(r'^email/$', weblate.accounts.views.email_login, name='email_login'),
    #url(r'', include(social_urls, namespace='social')),
]
