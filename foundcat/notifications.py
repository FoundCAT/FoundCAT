# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from weblate.trans.models import Change, Unit
from foundcat.models import ProfileExtension
from django.db.models import Q
from weblate.accounts.notifications import send_new_comment, send_any_translation, send_mails

def send_notification(unit, user):

   #print "Sending notifications"

   last_changes = unit.change_set.content()
   
   if len(last_changes) == 0:
        return

   change = last_changes[0]

   is_comment = change.action == change.ACTION_COMMENT
   is_suggestion = change.action == change.ACTION_SUGGESTION

   #is_translation = change.action in change.ACTIONS_CONTENT

#   if is_comment or change.old == change.target:
#        return


   relevant_changes = Change.objects.prefetch().filter(
       ~Q(user_id = user.id),
       unit=unit
   ).exclude(user__id__isnull=True)

   user_ids = relevant_changes.values_list('user_id', flat=True).distinct().order_by()
   subscriptions = ProfileExtension.objects.filter(
       user_id__in = user_ids,
       subscribed_edited_units = True
   )


   #print subscriptions

   mails = []
   for subscription in subscriptions:
       if is_comment:
           #print "SEND IT"
           user = change.user
           comment = unit.get_comments().order_by('-timestamp')[0]
           mails.append(
               send_new_comment(subscription.user.profile, unit, comment, user)
           )
       else:
           oldunit = Unit(
             target = unit.target, 
             translated = True
           )

           #print oldunit.target

           mails.append(
               send_any_translation(subscription.user.profile, unit, oldunit)
           )
   #print mails

   send_mails(mails)

