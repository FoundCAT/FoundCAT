# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.apps import AppConfig
from django.db.models.signals import post_save

def profile_saved(sender, instance, **kwargs):
    from weblate.accounts.models import Profile
    from foundcat.models import ProfileExtension

    if sender == Profile:

      user = instance.user
      pe = ProfileExtension.objects.filter(user=user)
      if pe.count() == 0:
        ProfileExtension.objects.create(user=user)


class FoundCATConfig(AppConfig):
    name = 'foundcat'
    verbose_name = "FoundCAT"
    def ready(self):
      post_save.connect(profile_saved)
      pass
