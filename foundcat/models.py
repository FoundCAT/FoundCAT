# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.db import models
from django.contrib.auth.models import User
from weblate.trans.models.subproject import SubProject
from weblate.trans.models.project import Project
from weblate.trans.models.translation import Translation
from weblate.trans.models.change import Change
from django.conf import settings
from django.utils.text import slugify
from translate.convert.txt2po import txt2po

from git import Repo
from translate.storage.po import pofile
from translate.storage.txt import TxtFile
from django.utils.encoding import python_2_unicode_compatible

import os

@python_2_unicode_compatible
class TextRepo(models.Model):
    FLAVOURS = (
        ('P', 'Plaintext'),
        ('W', 'Wikitext'),
        ('F', 'FoundCAT Wikitext'),
    )

    title = models.CharField(max_length=100)
    slug = models.CharField(max_length=100,unique=True)
    text = models.TextField(verbose_name='Segments')
    owner =  models.ForeignKey(User)
    repo =  models.ForeignKey(SubProject, null=True)
    flavour = models.CharField(max_length=2, choices=FLAVOURS, default='F')
    project = models.ForeignKey(Project)
    pub_date = models.DateTimeField('date published', null=True),
    create_date = models.DateTimeField('date created')

    source = models.TextField(verbose_name='Source text', null=True)

    def __str__(self):
        return self.title

    def git_dir(self):
	data_dir = settings.DATA_DIR.replace('/weblate/../data', '/data')
        translations_dir = os.path.join(data_dir, 'translations')
        user_dir = os.path.join(translations_dir, slugify(self.owner.id))
        return os.path.join(user_dir, "%s.git" % slugify(self.title))
    
    def locale_dir(self):
        return os.path.join(self.git_dir(), 'locale')
    
    def create_dir(self):
        git_dir = self.git_dir()
        locale_dir = self.locale_dir()
        if not os.path.exists(git_dir):
            os.makedirs(git_dir)
            Repo.init(git_dir)
            if not os.path.exists(locale_dir):
                os.makedirs(locale_dir)
                

    def source_txt(self):
        return os.path.join(self.git_dir(), 'source.txt')

    def po_template(self):
        return os.path.join(self.locale_dir(), self.project.source_language.code + '.pot')
            
    def write_source(self):
        text = u"{}\n\n{}".format(self.title, self.text)
    
        text_file = open(self.source_txt(), "w")
        text_file.write(text.encode('utf8'))
        text_file.close()
        
    def choose_input_store(self, inputfile):
        return TxtFile(inputfile, encoding="utf-8")
        
    def convert(self):
        inputfile  = open(self.source_txt(), "r")
        outputfile = open(self.po_template(), "w+")
        
        convertor = txt2po(duplicatestyle="msgctext")
        outputstore = convertor.convertstore(self.choose_input_store(inputfile))
        if outputstore.isempty():
            return 0
            
        outputstore.serialize(outputfile)

        outputfile.close()
        inputfile.close()
         

    def commit_changes(self):
        repo = Repo(self.git_dir())

        index = repo.index
        
        index.add('*')
        
        index.commit('Translation Repo created by foundcat script')
        
        
    def get_units(self):
        inputfile = open(self.po_template(), "r")
        inputstore = pofile(inputfile, encoding="utf-8")
        inputfile.close()
        return inputstore.units[1:]

    def save(self, *args, **kwargs):
        changed_source = False
        if getattr(self, '_title_changed', True):
            self.slug = slugify(self.title)
        if changed_source or getattr(self, '_text_changed', True):
            self.create_dir()
            self.write_source()
            self.convert()
            self.commit_changes()
        super(TextRepo, self).save(*args, **kwargs)

    @property
    def is_published(self):
        return self.repo is not None
        
    def get_translations(self):
        if self.is_published:
            return Translation.objects.filter(
                subproject = self.repo
            )
        
        
        
        
        
        
        
        
        
        
        

class ImprovementCategory(models.Model):
    title = models.TextField()
    pub_date = models.DateTimeField('date published', auto_now_add=True, null=True)
    pos = models.IntegerField(null=True)

    def __str__(self):
        return self.title



class ChangeImprovement(models.Model):
    improvement_category = models.ForeignKey(ImprovementCategory)
    change = models.ForeignKey(Change)


class ProfileExtension(models.Model):
    user = models.ForeignKey(User)
    subscribed_edited_units = models.BooleanField(default=True, verbose_name='Receive email notifications for changes on edited segments',)
