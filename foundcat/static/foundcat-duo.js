/*
Copyright © FoundCAT <team.foundcat@fh-aachen.de>

This file is part of FoundCAT <https://cat.fh-aachen.de/>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/



$(document).ready(function(){

  var $history = $('.foundcat-history');
  var count = $history.find('.list-unstyled > li').length;
  var max = 2;

  if(count > max + 1){
    $history.find('.list-unstyled li.clearfix:gt('+(max-1)+')').hide();
    $history.find('.more-changes').on('click', function(){
      $history.find('li').show();
      $(this).hide();
    });
  }
  else {
    console.log($history.find('.more-changes').hide());
  }




  
    var $textarea = $('.foundcat-segment.current .translation textarea');
    var old_text = $textarea.val();
    
    var update_buttons = function(){
        var $save = $('.foundcat-segment.current .fc-translate').hide();
        var $review = $('.foundcat-segment.current .fc-review').hide();
        var $next = $('.foundcat-segment.current .fc-next').hide();
        var new_text = $textarea.val();
        if(new_text != ''){
            if(old_text == new_text){
                $review.show();
            }
            else {
                $save.show();
            }
        }
        if($save.is(':hidden') || $review.is(':hidden')){
            $next.show();
        }
    };
    $('.btn.specialchar, .btn.copy-text').click(update_buttons);
    
    $('[data-toggle="btns"] .btn').on('click', function(){
        var $this = $(this);
        $this.parent().find('.active').removeClass('active');
        $this.addClass('active');
    });

    var highlight_spans_as_words = function(){
        $('#current-segment .source > .txt > span').each(function(){
            $('#id_source, #id_target').on('keyup change', function(){
                $('.submit-glossary').attr('disabled', false);
            });
            $(this).click(function(e){
                if(e.defaultPrevented){
                    return;
                }
                e.preventDefault();

                var source = $(this).data('source');
                var target = $(this).data('target');

                $('.nav-tabs a[href="#glossary').tab('show');
                
                if(source && target){
                    $('#id_source').val(source);
                    $('#id_target').val(target).select();
                    $('.submit-glossary').attr('disabled', true);
                }
                else{
                    var word = $(this).text().replace(/([,.!:]$)/g, "");
                    $('#id_source').val(word);
                    $('#id_target').val('').focus();
                    $('.submit-glossary').attr('disabled', false);
                }
                return false;
            });
        });
    };
    highlight_spans_as_words();


var loadMachineTranslations = function(data, textStatus) {
    decreaseLoading('#mt-loading');
    data.forEach(function (el, idx) {
        increaseLoading('#mt-loading');
        var urlprefix = $('#js-translate').attr('href');
        urlprefix += urlprefix.indexOf('?') >= 0 ? '&' : '?';
        urlprefix += 'service=' + el
        $.ajax({
            url: urlprefix,
            success: processMachineTranslation,
            error: failedMachineTranslation,
            dataType: 'json'
        });
    });
};
  
    processMachineTranslation = function(data) {
        decreaseLoading('#mt-loading');
        if (data.responseStatus === 200) {
            data.translations.forEach(function (el, idx) {
                
                var newRow = $('<div />').attr('class', 'foundcat-segment').data('quality', el.quality);
                var done = false;
                newRow.append($('<div/>').attr('class', 'source').text(el.source));
                newRow.append($('<div/>').attr('class', 'target').attr('lang', data.lang).attr('dir', data.dir)
                .append(
                    $('<div class="target-text" />').text(el.text)
                )
                .append(
                    $('<div class="machine-menu" />')
                        .append($('<a class="pull-right copymt btn btn-sm btn-default"><i class="fa fa-clipboard"></i> ' + gettext('Copy') + '<span class="mt-number text-info"></span></a>' ))
                        .append($('<div class="quality" />').text('Quality: ' + parseInt(el.quality)+'%'))
                        .append($('<div class="service" />').text('Service: ' + el.service))
                ));
                
                var $machineTranslations = $('#machinetranslation');
                $machineTranslations.children('.foundcat-segment').each(function (idx) {
                    if ($(this).data('quality') < el.quality && !done) {
                        $(this).before(newRow);
                        done = true;
                    }
                });
                if (! done) {
                    $machineTranslations.append(newRow);
                }
            });
            $('a.copymt').click(function () {
                var text = $(this).parent().parent().find('.target-text').text();
                var $text = $('.translation-editor').val(text).trigger('autosize.resize');
                $('#id_fuzzy').prop('checked', true);
                
                update_buttons();
                
            });

            for (var i = 1; i < 10; i++){
                Mousetrap.bindGlobal(
                    'alt+m ' + i,
                    function() {
                        return false;
                    }
                );
            }

            var $machineTranslations = $('#machinetranslation');
            $machineTranslations.children('.foundcat-segment').each(function (idx) {
                if (idx < 10) {
                    var key = getNumericKey(idx);
                    $(this).find('.mt-number').html(
                        ' <span class="badge kbd-badge" title="' +
                        interpolate(gettext('Alt+M then %s'), [key]) +
                        '">' +
                        key +
                        '</span>'
                    );
                    Mousetrap.bindGlobal(
                        'alt+m ' + key,
                        function(e) {
                            $($('#machinetranslation').children('.foundcat-segment')[idx]).find('a.copymt').click();
                            return false;
                        }
                    );
                } else {
                    $(this).find('.mt-number').html('');
                }
            });

        } else {
            var msg = interpolate(
                gettext('The request for machine translation using %s has failed:'),
                [data.service]
            );
            $('#mt-errors').append(
                $('<li>' + msg + ' ' + data.responseDetails + '</li>')
            );
        }
    };
  
    $('a[href="#machinetranslation"]').click(function(){
      if(!machineTranslationLoaded){
            machineTranslationLoaded = true;
            increaseLoading('#mt-loading');
            $.ajax({
                url: $('#js-mt-services').attr('href'),
                success: loadMachineTranslations,
                error: failedMachineTranslation,
                dataType: 'json'
            });
        }
    });
    if(window.location.hash == '#machinetranslation'){
      $('a[href="#machinetranslation"]').click();
    }


    $('.foundcat-search').click(function(){
      var q = $('#id_source').val() || $('#id_target').val();
      var url = 'http://www.linguee.de/deutsch-englisch/search?source=auto&query=' + encodeURIComponent(q);
      window.open(url, 'linguee');
    });
  
    $('.foundcat-add-dict-inline').submit(function () {
        var $form = $(this);
        increaseLoading('#glossary-add-loading');
        $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'json',
            success: function (data) {
                decreaseLoading('#glossary-add-loading');
                if (data.responseCode === 200) {
                    var $txt = $('#current-segment .txt');
                    
                    $txt.html(data.results);
                    highlight_spans_as_words();
                }
            },
            error: function (xhr, textStatus, errorThrown) {
                decreaseLoading('#glossary-add-loading');
            }
        });
        return false;
    });
    
    $('.foundcat-insert-glossary').click(function(e){
        e.preventDefault();
        var target = $('#id_target').val();
        insertEditor(target);
    });
  
    $('form.set-error-category select').change(function(e){
      $form = $(this).parents('form');
      increaseLoading('#errorcategory-add-loading');
      $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
            dataType: 'text',
            success: function (data) {
                decreaseLoading('#errorcategory-add-loading');
                var text = $('#id_improvementcategory option:selected').text();
                $('#comments .change-text + span').text('(Improvement: ' + text + ')');
            },
            error: function (xhr, textStatus, errorThrown) {
                decreaseLoading('#errorcategory-add-loading');
            }
        });
    });
  

    $('[data-toggle="tooltip"]').tooltip()


    $('.foundcat-segment.current').find(function(){

        var $text = $(this).find('textarea');


        update_buttons();

        $text.on('keyup', update_buttons);
        setTimeout(function(){
            $text.focus()
        }, 50);
    });
  
  
    var $nearby = $('#nearby-loading');
    var $last_link = $('#link-last-segment');
    $nearby.show();
    $last_link.hide();
    var loading = false;
    var load_more = function(){
        if($nearby.visible()){
            var $segments = $('.foundcat-segments');
            var $last = $segments.find('.foundcat-segment:last');
            var last_position = parseInt($last.data('position'));
            
            var total = $nearby.data('total');
            var base = $nearby.data('baseurl');
            
            
            
            
            if(last_position < total){
                var next_url = base + (last_position - 1) // -1 because to prevent the loaded segment from being highlighted as current segment
                console.log(next_url);
                if(!loading){
                  loading = true;
                  $.get(next_url, function(result){
                      var $new_segments = $('.foundcat-segments > .foundcat-segment', result);
                      $new_segments.each(function(num, new_segment){
                          $new_segment = $(new_segment);
                          var new_segment_position = $new_segment.data('position');
                          var segment_already_loaded = $segments.find('.foundcat-segment[data-position="' + new_segment_position + '"]').length == 1;
                          if(!segment_already_loaded){
                              $segments.append($new_segment.clone());
                          }
                      });
                      loading = false;
                      load_more();
                  });
                }
                
            }
            else{
                $nearby.hide();
            }
        }
    };
    load_more();
    $(document).scroll(load_more);
  
  
});
