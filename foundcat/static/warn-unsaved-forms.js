/*
Copyright © FoundCAT <team.foundcat@fh-aachen.de>

This file is part of FoundCAT <https://cat.fh-aachen.de/>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

var elems = [
    {
        form: '#commentform',
        input: 'input[name="comment"]'
    },
    {
        form: '#current-segment > .translation > form',
        input: 'textarea.translation-editor'
    }
];

$(function(){
    elems.forEach(function(elem){
        var $form = $(elem.form);
        $form.on('submit', function(e){
            elem.otext = $form.find(elem.input).val();
        });
        elem.otext = $form.find(elem.input).val();
    });

    $(window).on('beforeunload', function(){
        var ok = true;
        elems.forEach(function(elem){
            var $form = $(elem.form);
            var val = $form.find(elem.input).val();
            if(val != elem.otext){
                ok = false;
                return false;
            }
        });
        if(!ok){
          return 'Translation or comment won\'t be saved.';
        }
    });
});