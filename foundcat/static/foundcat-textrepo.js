/*
Copyright © FoundCAT <team.foundcat@fh-aachen.de>

This file is part of FoundCAT <https://cat.fh-aachen.de/>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

$(function(){
   var hash = document.location.hash.replace(/^#/, '')
   if(hash){
       $('.nav-tabs a[aria-controls="' + hash + '"').tab('show')
   }
   
   var segments = [];
   var connect_segments = function(){
       var i = $(this).data('id') - 1;
       segments[i]+= " " + segments[i+1]
       segments.splice(i+1, 1);
       $('#id_text').val(segments.join('\n\n'))
       segment_editor();
   };
   
   var divide_segment = function(){
       var pos = $(this).data('pos');
       
       var text = $('#id_text').val();
       var new_text = text.substr(0, pos).trim() + '\n\n' + text.substr(pos, text.length).trim()
       
       $('#id_text').val(new_text)
       segment_editor();
   };
   
   var segment_editor = function(){
       segments = $('#id_text').val().split('\n\n');
       
       var pos = 0;
       
       var $container = $('<div id="segment-container" class="list-group" />');
        for(var i = 0; i < segments.length; i++){
            var secretseparator = 'ssdgkjsdhgksjghljsjghsljensdg3kgssg4';
            var segment = segments[i].replace(/(\s\S)/g, secretseparator+'$1');
            var words = segment.split(secretseparator);
            var $words = $('<div class="words" />');
            for(var j = 0; j < words.length; j++){
                var word = words[j];
                if(j > 0){
                    $words.append(
                        $(' <span class="divide-segments" title="Divide segment"> <i class="fa fa-level-down" aria-hidden="true"></i></div>').data('pos', pos).click(divide_segment)
                    );
                }
                var $word = $('<span class="word">').text(word);
                $words.append($word);
                pos += word.length;
                
            }
            pos += 2;
            
            if(i > 0){
                $container.append(
                    $('<div class="connect-segments" title="Connect segments"><i class="fa fa-link" aria-hidden="true"></i></div>').data('id', i).click(connect_segments)
                );
            }
            $container.append(
                $('<div class="list-group-item" />').append($words)
            );
       }
       $('#segment-container').remove();
       $('#div_id_text').append($container)
   }
   segment_editor();
});
