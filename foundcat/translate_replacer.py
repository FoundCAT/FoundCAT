# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from foundcat.models import TextRepo
from weblate.trans.models import Unit

def replace_translation(units, source):
    result = ""
    indexpointer = 0
    lastIndex = 0
    currentIndex = 0

    if len(units) > 1:
        #result = units[0].target first element in units is title of the text/ component

        for unit in units[1:]:
            lastIndex = currentIndex
            currentIndex = source.find(unit.source, currentIndex)
            result += source[lastIndex:currentIndex]
            currentIndex += len(unit.source)
            result += unit.target

    return result
