# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from weblate.trans.util import render

from foundcat.forms import TextRepoForm, TextRepoAddForm, TextRepoWikiForm
from foundcat.models import TextRepo

from weblate.trans.views.helper import (get_project)
from django.shortcuts import redirect, reverse

from django.utils.text import slugify
from weblate.trans.models.subproject import SubProject as Component

import datetime

from django.forms.utils import ErrorList
from django.db import IntegrityError

from foundcat.foundcat_segmenter import split_wikitext
from django.contrib import messages

import urllib2
import json
import urllib
import re

def add(request, project):
    
    p = get_project(request, project)

    form = TextRepoAddForm()

    mine = TextRepo.objects.filter(
        owner = request.user,
        project = p
    ).order_by('-create_date')


    return render(
        request,
        'add.html',
        {
            'myrepos': mine,
            'object': p,
            'form': form
        }
    )
    

    
    
    
    
    
def add_wiki(request, project):
    
    p = get_project(request, project)

    wiki_query = request.GET.get('wiki_query')
    code = p.source_language.code.replace('_GB', '')

    if wiki_query:
        form = TextRepoForm()
        try:
            wikiurl = re.compile(r"^https://(de|en)\.wikipedia\.org/wiki/")
            if wikiurl.search(wiki_query):
                wiki_query = re.sub(wikiurl, '', wiki_query)
                title = urllib.unquote(wiki_query).replace('_', ' ').encode('utf-8')
                encoded_title = wiki_query
            else:
                encoded_wiki_query = urllib.quote(wiki_query.encode('utf-8'),safe='')
                search_url = 'https://'+code+'.wikipedia.org/w/api.php?action=query&list=search&srsearch={}&utf8=&format=json'.format(encoded_wiki_query)
                search = urllib2.urlopen(search_url)
                search_data = json.load(search)
                title = search_data["query"]["search"][0]["title"].encode('utf-8')
                encoded_title = urllib.quote(title,safe='')
                
            result_url=u"https://{}.wikipedia.org/w/api.php?action=query&titles={}&prop=revisions&rvprop=content&format=json".format(code, encoded_title)
            page = urllib2.urlopen(result_url)
            page_data = json.load(page)
            content = page_data["query"]["pages"].itervalues().next()["revisions"][0]["*"]

            
            segments = split_wikitext(content);
            
            
            form.fields["title"].initial  = title
            form.fields["source"].initial = content
            form.fields["text"].initial = '\n\n'.join(segments)

            return render(
                request,
                'repo.add.html',
                {
                    'object': p,
                    'form': form,
                    'title': title
                }
            )
            
        except (IndexError):
            
            messages.error(request, "Couldn't find a matching Wikipedia page.")
            return redirect(reverse('repo_add', kwargs={'project':project}) + '#wikisource')

    else:
        form = TextRepoAddForm()

    mine = TextRepo.objects.filter(
        owner = request.user,
        project = p
    ).order_by('-create_date')


    return render(
        request,
        'repo.add.html',
        {
            'myrepos': mine,
            'object': p,
            'form': form
        }
    )
    

    
    
    
def edit(request, project, subproject):
    repo = TextRepo.objects.get(
        slug=subproject,
    )
    
    #if repo.is_published:
    #    return redirect('duo', project=project, subproject=subproject, lang = 'en_GB')
    
    if repo.owner != request.user:
        return redirect('repo_add', project=project)
    
    owner=request.user
    
    p = get_project(request, project)
	
    if request.POST and 'delete' in request.POST:
        repo.delete()
        messages.success(request, 'Your source text was deleted')
        return redirect('repo_add', project=p.slug)
		
    
    form = TextRepoForm(request.POST or None, instance=repo)
    if request.POST and form.is_valid():
        if repo.is_published:
            return redirect('translate', project=project, subproject=subproject, lang = 'en_GB')
        elif 'publish' in request.POST:
            try:
                component = Component.objects.create(
                    name = repo.title,
                    slug = repo.slug,
                    project = p,
                    repo = repo.git_dir(),
                    filemask = 'locale/*',
                    new_base = 'locale/'+ p.source_language.code +'.pot',
                    edit_template = False,
                    file_format = 'po',
                    enable_suggestions = True,
                    suggestion_voting = True
                )
                repo.repo = component
                repo.pub_date = datetime.datetime.now()
                repo.save()
                
                messages.success(request, 'Your source text has been published.')
                return redirect('translate', project=project, subproject=subproject, lang = 'en_GB')
        
            except IntegrityError as e:
                errors = form._errors.setdefault("title", ErrorList())
                errors.append(u"This title may already be in use. Please choose another title.")
        else:
            try:
                form.save()
                messages.success(request, 'Your changes have been saved.')
                return redirect('repo_edit', project=project, subproject=slugify(request.POST.get('title')))
            except IntegrityError as e:
                errors = form._errors.setdefault("title", ErrorList())
                errors.append(u"This title may already be in use. Please choose another title.")
    
    
    
    return render(
        request,
        'repo.edit.html',
        {
            'object': p,
            'repo': repo,
            'form': form
        }
    )
    
def upload(request, project):

    p = get_project(request, project)

    title = request.POST.get('title')
    source = request.POST.get('source')
    text = request.POST.get('text', '')
    if text == '':
        text = '\n\n'.join(split_wikitext(source))
    
    data = {
        'title': title,
        'source': source,
        'text': text
    }
    
    form = TextRepoForm(data)
    
    if request.method == 'POST' and form.is_valid():
        repo = TextRepo()
        repo.title = title
        repo.project = p
        repo.source = source
        repo.text = text
        repo.owner=request.user
        repo.pub_date = datetime.datetime.now()
        repo.create_date = datetime.datetime.now()
        repo.slug = slugify(title)
        
        try:
            if 'edit' in request.POST:
                repo.save();
                messages.success(request, 'Your changes have been saved.')
                return redirect('repo_edit', project=p.slug, subproject=repo.slug)

            elif 'publish' in request.POST:
                repo.save() # would be enough to only save to disk because it needs to be updated with the component later anyway
                component = Component(
                    name = repo.title,
                    slug = repo.slug,
                    project = p,
                    repo = repo.git_dir(),
                    filemask = 'locale/*',
                    new_base = 'locale/'+p.source_language.code+'.pot',
                    edit_template = False,
                    file_format = 'po',
                    enable_suggestions = True,
                    suggestion_voting = True

                )
                component.save()
                
                repo.repo = component
                repo.save()
                
                messages.success(request, 'Your source text has been uploaded. Just select one or more languages to translate into.')
                return redirect('foundcat-add-lang', project=p.slug, subproject=component.slug)

            
        except IntegrityError as e:
        
            #form = TextRepoForm(instance=repo)
            form.add_error('title', 'This title may already be in use. Please choose another title.')
            #errors = form._errors.setdefault("title", ErrorList())
            #errors.append(u"Sorry, this text can't be published since a component with the exact name already exists")

    return render(
        request,
        'repo.edit.wiki.html',
        {
            'object': p,
            'form': form,
            'title': title,
        }
    )
    

    
    
    
    
    

# 
def upload_new_text(request, project):
    pass


def upload_wiki(request, project):

    p = get_project(request, project)
    
    form = TextRepoForm(request.POST)

    if request.POST and form.is_valid():
        try:
                            
            textrepo = form.save(commit=False)
            textrepo.owner = request.user
            textrepo.project = p
            textrepo.slug = slugify(request.POST.get('title'))
            textrepo.pub_date = datetime.datetime.now()
            textrepo.create_date = datetime.datetime.now()
            textrepo.save();
            

            component = Component(
                name = textrepo.title,
                slug = textrepo.slug,
                project = p,
                repo = textrepo.git_dir(),
                filemask = 'locale/*',
                new_base = 'locale/'+p.source_language.code+'.pot',
                edit_template = False,
                file_format = 'po',
                enable_suggestions = True,
                suggestion_voting = True

            )
            component.save()
            
            textrepo.repo = component
            
            
            textrepo.save()
            

            messages.success(request, 'The Wikipedia page has been published.')
            return redirect('foundcat-add-lang', project=project, subproject=textrepo.slug)

        except IntegrityError as e:            
            errors = form._errors.setdefault("title", ErrorList())
            errors.append(u"This title may already be in use. Please choose another title.")
                         
            return render(
                request,
                'repo.add.html',
                {
                    'object': p,
                    'form': form,
                    'title': textrepo.title
                }
            )



def change_text(request, project, repo_id):
    pass
