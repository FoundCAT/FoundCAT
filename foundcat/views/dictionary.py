# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



import csv
import sys

from django.shortcuts import get_object_or_404, redirect
from django.utils.encoding import force_text
from django.utils.translation import ugettext as _, ungettext
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.template.loader import render_to_string

import six
from six.moves.urllib.parse import urlencode

from weblate.utils import messages
from weblate.trans.exporters import get_exporter
from weblate.trans.models import Translation, Dictionary, Change, Unit
from weblate.lang.models import Language
from weblate.trans.site import get_site_url
from weblate.utils.errors import report_error
from weblate.trans.util import render
from weblate.trans.forms import WordForm, DictUploadForm, LetterForm
from weblate.permissions.helpers import (
    can_add_dictionary, can_upload_dictionary, can_delete_dictionary,
    can_change_dictionary, check_access,
)
from weblate.trans.views.helper import get_project, import_message


def add_dictionary(request, unit_id):
    unit = get_object_or_404(Unit, pk=int(unit_id))
    check_access(request, unit.translation.subproject.project)

    prj = unit.translation.subproject.project
    lang = unit.translation.language

    code = 403
    results = ''

    if request.method == 'POST' and can_add_dictionary(request.user, prj):
        form = WordForm(request.POST)
        if form.is_valid():
            Dictionary.objects.create(
                request.user,
                project=prj,
                language=lang,
                source=form.cleaned_data['source'],
                target=form.cleaned_data['target']
            )
            code = 200
            results = render_to_string(
                'foundcat-source-glossary.html',
                {
                    'glossary': Dictionary.objects.get_words(unit),
                    'item': unit,
                    'user': request.user,
                }
            )

    return JsonResponse(
        data={'responseCode': code, 'results': results}
    )
