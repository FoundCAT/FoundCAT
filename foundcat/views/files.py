# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from django.http import HttpResponse, Http404
from django.shortcuts import get_object_or_404
import django.utils.translation
from django.utils.translation import trans_real, ugettext as _

from weblate.utils import messages
from weblate.permissions.helpers import check_access
from weblate.trans.exporters import get_exporter
from weblate.trans.models import Project, SubProject, Translation, Unit
from translate.storage.tbx import tbxfile
from translate.storage.tmx import tmxfile

from weblate.lang.models import Language

from weblate.trans.exporters import BaseExporter
from foundcat.models import TextRepo

from translate.convert.po2txt import po2txt
from translate.storage.txt import TxtFile
from weblate.trans.views.helper import get_translation
from django.utils.text import slugify
from foundcat.translate_replacer import replace_translation

class TranslationMemoryExporter(BaseExporter):
    content_type = 'application/x-tmx'
    extension = 'tmx'

    def __init__(self, project=None, language=None, url=None, **kwargs):
        self.project = project
        self.language = language
        self.url = url
        self.storage = self.get_storage()
        
        self.storage.setsourcelanguage(
            self.project.source_language.code
        )
        self.storage.settargetlanguage(
            self.language.code
        )
        
    def get_storage(self):
        store = tmxfile(sourcelanguage=self.project.source_language.code,targetlanguage=self.language.code)
        return store
        
        
    def add_unit(self, unit):
        output = self.storage.UnitClass(
            self.handle_plurals(unit.get_source_plurals())
        )
        #output.target = self.handle_plurals(unit.get_target_plurals())
        
        output.setsource(self.handle_plurals(unit.get_source_plurals()), self.project.source_language.code)
        output.settarget(self.handle_plurals(unit.get_target_plurals()), self.language.code)
        
        context = self.string_filter(unit.context)
        if context:
            output.setcontext(context)
        for location in unit.location.split():
            if location:
                output.addlocation(location)
        note = self.string_filter(unit.comment)
        if note:
            output.addnote(note)
        if hasattr(output, 'settypecomment'):
            for flag in unit.flags.split(','):
                if flag:
                    output.settypecomment(flag)
        if unit.fuzzy:
            output.markfuzzy(True)
        self.storage.addunit(output)


class SourceExporter(BaseExporter):
    content_type = 'text/plain'
    extension = 'txt'

    def __init__(self, subproject=None, language=None, repo=None, **kwargs):
        self.repo = repo
        self.subproject=subproject
        self.language=language
        
    
    def get_response(self, filetemplate='{title}-{language}.{extension}'):
        filename = filetemplate.format(
            title=self.subproject.slug,
            language=self.language.code,
            extension=self.extension
        )

        response = HttpResponse(
            content_type='{0}; charset=utf-8'.format(self.content_type)
        )
        response['Content-Disposition'] = 'attachment; filename={0}'.format(
            filename
        )

        # Save to response
        response.write(self.repo.text)

        return response

        
class textrepo2txt(object):
    """po2txt can take a po file and generate txt.
    best to give it a template file otherwise will just concat msgstrs
    """

    def __init__(self, wrap=None):
        self.wrap = wrap

    def wrapmessage(self, message):
        """rewraps text as required"""
        if self.wrap is None:
            return message
        return "\n".join([textwrap.fill(line, self.wrap, replace_whitespace=False)
                          for line in message.split("\n")])

    def mergestore(self, inputstore, templatetext, includefuzzy):
        """converts a file to txt format"""
        txtresult = templatetext
        # TODO: make a list of blocks of text and translate them individually
        # rather than using replace
        for unit in inputstore.units:
            if not unit.istranslatable():
                continue
            if not unit.isfuzzy() or includefuzzy:
                txtsource = unit.source
                txttarget = self.wrapmessage(unit.target)
                if unit.istranslated():
                    txtresult = txtresult.replace(txtsource, txttarget)
        return txtresult


class TranslationExporter(BaseExporter):
    content_type = 'text/plain'
    extension = 'txt'

    def __init__(self, translation=None, repo=None, fuzzy=False, hideincomplete=True, **kwargs):
        self.translated_title = 'untranslated'
        self.fuzzy = fuzzy
        self.hideincomplete=hideincomplete
        self.repo = repo
        self.project=translation.subproject.project
        self.subproject=translation.subproject
        self.language=translation.language
        #self.storage = self.get_storage()
        
        self.units = Unit.objects.prefetch().filter(
            translation = translation
        )

        
        """self.units = Unit.objects.prefetch().filter(
            translation = translation
        )"""
        
    #def get_storage(self):
    #    store = TxtFile(encoding='utf-8')
    #    store.parse(self.repo.text.split(u"\n"))
    #    return store
    
    def translate(self):
        return replace_translation(self.units, self.repo.source)
    
    def get_response(self, filetemplate='{title}-{language}.{extension}'):
        result = self.translate()
    
        title = slugify(self.translated_title)
    
        filename = filetemplate.format(
            title="{}__{}".format(self.subproject.slug, title),
            language=self.language.code,
            extension=self.extension
        )

        response = HttpResponse(
            content_type='{0}; charset=utf-8'.format(self.content_type)
        )
        response['Content-Disposition'] = 'attachment; filename={0}'.format(
            filename
        )

        # Save to response
        response.write(result)

        return response
        



def download_tm(request, project, lang):
    project_obj = get_object_or_404(
        Project,
        slug=project
    )
    language_obj = get_object_or_404(Language, code=lang)

    exporter = TranslationMemoryExporter(project=project_obj, language=language_obj)
    
    translations = Translation.objects.prefetch().filter(
        enabled=True,
        subproject__project = project_obj
    )
    units = Unit.objects.prefetch().filter(
        translation__in = translations
    )
    
    for unit in units:
        if unit.translated and not unit.fuzzy:
            exporter.add_unit(unit)
    
    return exporter.get_response(
        '{{project}}-{{language}}.{{extension}}'.format()
    )

    
def download_source(request, project, subproject):
    subproject_obj = get_object_or_404(
        SubProject.objects.prefetch(),
        slug = subproject,
        project__slug = project
    )
    
    textrepo = get_object_or_404(
        TextRepo,
        repo = subproject_obj
    )
    
    exporter = SourceExporter(subproject=subproject_obj, language=subproject_obj.project.source_language, repo=textrepo)

    return exporter.get_response(
        '{{title}}-{{language}}.{{extension}}'.format()
    )

    
def download_translation(request, project, subproject, lang):
    subproject_obj = get_object_or_404(
        SubProject.objects.prefetch(),
        slug = subproject,
        project__slug = project
    )
    
    translation = get_translation(request, project, subproject, lang)
    
    textrepo = get_object_or_404(
        TextRepo,
        repo = subproject_obj
    )
    
    exporter = TranslationExporter(translation=translation, repo=textrepo, fuzzy=False, hideincomplete=True)
    

    return exporter.get_response(
        '{{title}}-{{language}}.{{extension}}'.format()
    )
