
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Sum, Count, F
from django.http import HttpResponse
from django.shortcuts import redirect
from django.utils import translation
from django.views.decorators.cache import never_cache
from django.utils.encoding import force_text
from django.utils.html import escape
from django.utils.http import urlencode
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.utils.translation.trans_real import parse_accept_lang_header
import django.views.defaults

from weblate.utils import messages
from weblate.trans.models import (
    Project, Translation, Check, ComponentList, Change, Unit, IndexUpdate,
)
from weblate.requirements import get_versions, get_optional_versions
from weblate.lang.models import Language
from weblate.trans.forms import (
    get_upload_form, SearchForm, SiteSearchForm,
    AutoForm, ReviewForm, get_new_language_form,
    ReportsForm, ReplaceForm, 
)
from weblate.permissions.helpers import (
    can_automatic_translation, can_translate,
)
from weblate.accounts.models import Profile
from weblate.accounts.notifications import notify_new_language
from weblate.trans.stats import get_per_language_stats
from weblate.trans.views.helper import (
    get_project, get_subproject, get_translation,
    try_set_language,
)
from weblate.trans.util import (
    render, sort_objects, sort_unicode, translation_percent,
)
import weblate


@never_cache
@login_required
def new_language(request, project, subproject):
    obj = get_subproject(request, project, subproject)

    form_class = get_new_language_form(request, obj)

    if request.method == 'POST':
        form = form_class(obj, request.POST)
#        form.helper.form_action = 'test'

        last_lang = 'en_GB'

        if form.is_valid():
            langs = form.cleaned_data['lang']
            for language in Language.objects.filter(code__in=langs):
                last_lang=language
                if obj.new_lang == 'contact':
                    notify_new_language(obj, language, request.user)
                    messages.success(
                        request,
                        _(
                            "A request for a new translation has been "
                            "sent to the project's maintainers."
                        )
                    )
                elif obj.new_lang == 'add':
                    obj.add_new_language(language, request)
            return redirect(reverse('translate', kwargs={'project':obj.project.slug, 'subproject':obj.slug, 'lang':last_lang.code}))
        else:
            messages.error(
                request,
                _('Please fix errors in the form.')
            )
    else:
        form = form_class(obj)

    return render(
        request,
        'foundcat-add-lang.html',
        {
            'object': obj,
            'project': obj.project,
            'form': form,
        }
    )


