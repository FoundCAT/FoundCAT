# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Original author: Michal Čihař <michal@cihar.com> part of Weblate
# <https://weblate.org/> license: GPLv3+


from __future__ import unicode_literals

import copy
import uuid
import time

from django.shortcuts import get_object_or_404, redirect
from django.views.decorators.http import require_POST
from django.utils.translation import ugettext as _, ungettext
from django.utils.encoding import force_text
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.utils import formats
from django.core.exceptions import PermissionDenied

from weblate.utils import messages
from weblate.permissions.helpers import check_access
from weblate.trans.models import (
    Unit, Change, Comment, Suggestion, Dictionary,
    get_related_units,
)
from weblate.trans.autofixes import fix_target
from django.forms import CharField, Textarea, HiddenInput
import django.forms as forms
from weblate.trans.forms import (
    TranslationForm, SearchForm, InlineWordForm,
    MergeForm, AutoForm, ReviewForm, ReplaceForm,
    AntispamForm, CommentForm, RevertForm
)
from weblate.trans.views.helper import (
    get_translation, get_subproject, get_project, import_message,
    show_form_errors,
)
from weblate.trans.checks import CHECKS
from weblate.trans.util import join_plural, render, redirect_next
from weblate.trans.autotranslate import auto_translate
from weblate.permissions.helpers import (
    can_translate, can_suggest, can_accept_suggestion, can_delete_suggestion,
    can_vote_suggestion, can_delete_comment, can_automatic_translation,
    can_add_comment,
)
from weblate.utils.hash import checksum_to_hash

from weblate.trans.views.edit import (search, handle_suggestions, perform_suggestion, handle_revert)   # , handle_translate , perform_translation

from foundcat.forms import FeedbackForm
from foundcat.models import ImprovementCategory, TextRepo, ChangeImprovement


from weblate.trans.models.change import Change
from weblate.accounts.models import Profile
from django.db.models import Q
from weblate.accounts.notifications import send_new_comment, send_any_translation, send_mails

from foundcat.notifications import send_notification

#from weblate.utils.state import (
#    STATE_TRANSLATED, STATE_FUZZY, STATE_APPROVED, STATE_EMPTY,
#)

def perform_translation(unit, form, request):
    """Handle translation and stores it to a backend."""
    # Remember old checks
    oldchecks = set(
        unit.active_checks().values_list('check', flat=True)
    )

    # Run AutoFixes on user input
    if not unit.translation.is_template():
        new_target, fixups = fix_target(form.cleaned_data['target'], unit)
    else:
        new_target = form.cleaned_data['target']
        fixups = []

    old_unit = Unit.objects.get(id=form.cleaned_data['unit'].id)

    is_fuzzy = True

    if old_unit.target == '':
      # Save
      saved = unit.translate(
          request,
          new_target,
          is_fuzzy
      )

    
    Suggestion.objects.add(
       unit, new_target[0], request
    )

    
    if [old_unit.target] != new_target and (old_unit.translated or old_unit.fuzzy):
        messages.info(
            request,
            _('You can leave feedback why you edited this segment.')
        )
        return False
        

    # Warn about applied fixups
    if len(fixups) > 0:
        messages.info(
            request,
            _('Following fixups were applied to translation: %s') %
            ', '.join([force_text(f) for f in fixups])
        )

    # Get new set of checks
    newchecks = set(
        unit.active_checks().values_list('check', flat=True)
    )

    # Did we introduce any new failures?
    if saved and newchecks > oldchecks:
        # Show message to user
        messages.error(
            request,
            _(
                'Some checks have failed on your translation: {0}'
            ).format(
                ', '.join(
                    [force_text(CHECKS[check].name) for check in newchecks]
                )
            )
        )
        # Stay on same entry
        return False

        send_notification(unit, request.user)


    return True

def handle_translate(translation, request, user_locked,
                     this_unit_url, next_unit_url):
    """Save translation or suggestion to database and backend."""
    # Antispam protection
    antispam = AntispamForm(request.POST)
    if not antispam.is_valid():
        # Silently redirect to next entry
        return HttpResponseRedirect(next_unit_url)

    # Check whether translation is not outdated
    translation.check_sync()

    form = TranslationForm(
        request.user.profile, translation, None, request.POST
    )
    if not form.is_valid():
        show_form_errors(request, form)
        return

    unit = form.cleaned_data['unit']
    go_next = True
    
    original = request.POST.get('original', None)
    
    if original and unit.target != original:
        go_next = perform_translation(unit, form, request)
        messages.error(
            request,
            _('Someone edited this segment while you were translating. Please check!')
        )

    elif 'suggest' in request.POST:
        go_next = perform_suggestion(unit, form, request)
    elif not can_translate(request.user, unit.translation):
        messages.error(
            request,
            _('You don\'t have privileges to save translations!')
        )
    elif not user_locked:
        # Custom commit message
        message = request.POST.get('commit_message')
        if message is not None and message != unit.translation.commit_message:
            # Commit pending changes so that they don't get new message
            unit.translation.commit_pending(request, request.user)
            # Store new commit message
            unit.translation.commit_message = message
            unit.translation.save()

        go_next = perform_translation(unit, form, request)

        
        
    # Redirect to next entry
    if go_next:
        return HttpResponseRedirect(next_unit_url)
    else:
        return HttpResponseRedirect(this_unit_url)

from weblate.trans.forms import (PluralField, ChecksumForm)
from django.db.models import F

from django.utils.translation import (
    ugettext_lazy as _, ugettext, pgettext_lazy, pgettext
)


from weblate.trans.forms import PluralField, PluralTextarea, ChecksumForm, COPY_TEMPLATE, BUTTON_TEMPLATE, ICON_TEMPLATE, RADIO_TEMPLATE, TOOLBAR_TEMPLATE
from django.utils.html import escape
import json
from weblate.trans.specialchars import get_special_chars
from django.utils.safestring import mark_safe

from django.conf import settings

EDITOR_TEMPLATE = '''
{0}<label for="{1}">{2}</label>
{3}
'''

GROUP_TEMPLATE = '''
<div class="btn-group btn-group-sm fc-toolbar-group" {0}>{1}</div>
'''



class FoundcatPluralTextarea(Textarea):
    """Text area extension which possibly handles plurals."""
    def __init__(self, *args, **kwargs):
        self.profile = None
        super(FoundcatPluralTextarea, self).__init__(*args, **kwargs)

    def get_toolbar(self, language, fieldname, unit, idx):
        """Return toolbar HTML code."""
        profile = self.profile
        groups = []
        plurals = unit.get_source_plurals()
        if idx and len(plurals) > 1:
            source = plurals[1]
        else:
            source = plurals[0]
        # Copy button
        extra_params = COPY_TEMPLATE.format(
            escape(ugettext('Loading…')),
            unit.checksum,
            escape(json.dumps(source))
        )
        groups.append(
            GROUP_TEMPLATE.format(
                '',
                BUTTON_TEMPLATE.format(
                    'copy-text',
                    ugettext('Fill in with source string'),
                    extra_params,
                    ICON_TEMPLATE.format('clipboard', ugettext('Copy'))
                )
            )
        )

        # Special chars
        chars = [
            BUTTON_TEMPLATE.format(
                'specialchar',
                name,
                'data-value="{}"'.format(
                    value.encode('ascii', 'xmlcharrefreplace').decode('ascii')
                ),
                char
            )
            for name, char, value in
            get_special_chars(language, profile.special_chars)
        ]

        groups.append(
            GROUP_TEMPLATE.format('', '\n'.join(chars))
        )

        # RTL/LTR switch
        if language.direction == 'rtl':
            rtl_name = 'rtl-{0}'.format(fieldname)
            rtl_switch = [
                RADIO_TEMPLATE.format(
                    'direction-toggle active',
                    ugettext('Toggle text direction'),
                    rtl_name,
                    'rtl',
                    'checked="checked"',
                    'RTL',
                ),
                RADIO_TEMPLATE.format(
                    'direction-toggle',
                    ugettext('Toggle text direction'),
                    rtl_name,
                    'ltr',
                    '',
                    'LTR'
                ),
            ]
            groups.append(
                GROUP_TEMPLATE.format(
                    'data-toggle="buttons"',
                    '\n'.join(rtl_switch)
                )
            )

        return TOOLBAR_TEMPLATE.format('\n'.join(groups))

    def render(self, name, value, attrs=None, **kwargs):
        """Render all textareas with correct plural labels."""
        unit = value
        values = unit.get_target_plurals()
        lang = unit.translation.language
        tabindex = self.attrs['tabindex']

        # Need to add extra class
        attrs['class'] = 'translation-editor form-control'
        attrs['tabindex'] = tabindex
        attrs['lang'] = lang.code
        attrs['dir'] = lang.direction
        attrs['rows'] = 3

        # Okay we have more strings
        ret = []
        base_id = 'id_{0}'.format(unit.checksum)
        for idx, val in enumerate(values):
            # Generate ID
            fieldname = '{0}_{1}'.format(name, idx)
            fieldid = '{0}_{1}'.format(base_id, idx)
            attrs['id'] = fieldid
            attrs['tabindex'] = tabindex + idx

            # Render textare
            textarea = super(FoundcatPluralTextarea, self).render(
                fieldname,
                val,
                attrs,
                **kwargs
            )
            # Label for plural
            if len(values) == 1:
                if unit.translation.is_template():
                    label = ugettext('Source')
                else:
                    label = ugettext('Translation')
            else:
                label = lang.get_plural_label(idx)
            ret.append(
                textarea
            )

        # Show plural equation for more strings
        if len(values) > 1:
            ret.append(
                PLURALS_TEMPLATE.format(
                    get_doc_url('user/translating', 'plurals'),
                    ugettext('Documentation for plurals.'),
                    '<abbr title="{0}">{1}</abbr>: {2}'.format(
                        ugettext(
                            'This equation identifies which plural form '
                            'will be used based on given count (n).'
                        ),
                        ugettext('Plural equation'),
                        lang.pluralequation
                    )
                )
            )

        # Join output
        return mark_safe(''.join(ret))

    def value_from_datadict(self, data, files, name):
        """Return processed plurals as a list."""
        ret = []
        for idx in range(0, 10):
            fieldname = '{0}_{1:d}'.format(name, idx)
            if fieldname not in data:
                break
            ret.append(data.get(fieldname, ''))
        ret = [smart_text(r.replace('\r', '')) for r in ret]
        return ret



class FoundcatPluralField(CharField):
    """Renderer for the plural field.

    The only difference from CharField is that it does not force value to
    be string.
    """
    def __init__(self, max_length=None, min_length=None, *args, **kwargs):
        kwargs['label'] = ''
        super(FoundcatPluralField, self).__init__(
            *args,
            widget=FoundcatPluralTextarea,
            **kwargs
        )

    def to_python(self, value):
        """Return list or string as returned by PluralTextarea."""
        return value


class FoundcatTranslationForm(ChecksumForm):
    """Form used for translation of single string."""
    target = FoundcatPluralField(
        required=False,
    )
    original = forms.CharField(widget=forms.HiddenInput())

    def __init__(self, profile, translation, unit,
                 *args, **kwargs):
        if unit is not None:
            kwargs['initial'] = {
                'checksum': unit.checksum,
                'target': unit
            }
            kwargs['auto_id'] = 'id_{0}_%s'.format(unit.checksum)
        tabindex = kwargs.pop('tabindex', 100)
        super(FoundcatTranslationForm, self).__init__(
            translation, *args, **kwargs
        )
        self.fields['target'].widget.attrs['tabindex'] = tabindex
        self.fields['target'].widget.profile = profile
        self.fields['original'].initial = unit.target

        
def set_error_category(request, change_id):
    c = get_object_or_404(Change, pk=change_id, user=request.user)
    ic = get_object_or_404(ImprovementCategory, pk=request.POST.get('improvementcategory'))
    
    new_change_improvement = ChangeImprovement.objects.create(improvement_category=ic, change = c)
    #new_change_improvement.improvement_category = ic
    #new_change_improvement.change = c
    #new_change_improvement.save()
    
    return HttpResponse('', content_type='application/json')

        
        
def translate(request, project, subproject, lang):
    """Generic entry point for translating, suggesting and searching."""
    translation = get_translation(request, project, subproject, lang)

    # Check locks
    user_locked = translation.is_user_locked(request.user)
    project_locked = translation.subproject.locked
    locked = project_locked or user_locked

    # Search results
    search_result = search(translation, request)

    # Handle redirects
    if isinstance(search_result, HttpResponse):
        return search_result

    # Get numer of results
    num_results = len(search_result['ids'])

    # Search offset
    offset = search_result['offset']

    # Checksum unit access
    if search_result['checksum']:
        try:
            unit = translation.unit_set.get(id_hash=search_result['checksum'])
            offset = search_result['ids'].index(unit.id)
        except (Unit.DoesNotExist, ValueError):
            messages.warning(request, _('No string matched your search!'))
            return redirect(translation)
    '''try:
        offset = int(request.GET.get('offset', search_result.get('offset', 0)))
    except ValueError:
        offset = 0'''
    

    # Check boundaries
    if not 0 <= offset < num_results:
        messages.info(request, _('You have reached end of translating.'))
        # Delete search
        del request.session[search_result['key']]
        # Redirect to translation
        return redirect(translation.get_translate_url())
        

    # Some URLs we will most likely use
    base_unit_url = '{0}?{1}&offset='.format(
        translation.get_translate_url(),
        search_result['url']
    )
        
    this_unit_url = base_unit_url + str(offset)
    next_unit_url = base_unit_url + str(offset + 1)

    response = None

    # Any form submitted?
    if request.method == 'POST' and not project_locked:
        if any(key in request.POST for key in ['accept', 'accept_edit', 'delete', 'upvote', 'downvote']) and not locked:
            response = handle_suggestions(
                translation, request, this_unit_url, next_unit_url,
            )
        else:
          response = handle_translate(translation, request, user_locked, this_unit_url, next_unit_url)

    # Handle translation merging
    elif 'merge' in request.GET and not locked:
        response = handle_merge(
            translation, request, next_unit_url
        )

    # Handle reverting
    elif 'revert' in request.GET and not locked:
        response = handle_revert(
            translation, request, this_unit_url
        )

    # Grab actual unit
    try:
        unit = translation.unit_set.get(pk=search_result['ids'][offset])
    except Unit.DoesNotExist:
        # Can happen when using SID for other translation
        messages.error(request, _('Invalid search string!'))
        return redirect(translation)

    # Show secondary languages for logged in users
    if request.user.is_authenticated:
        secondary = unit.get_secondary_units(request.user)
    else:
        secondary = None

    # Spam protection
    antispam = AntispamForm()

    # Prepare form
    form = FoundcatTranslationForm(request.user.profile, translation, unit)

    others = Unit.objects.same(unit, False)
    # Is it only this unit?
    if others.count() == 1:
        others = Unit.objects.none()

    last_changes = unit.change_set.order_by('-timestamp')

    # Pass possible redirect further
    if response is not None:
        return response

    change_list = []
    
    ic = None
    if len(last_changes) > 0:
      cis = ChangeImprovement.objects.filter(change__in=last_changes)

      count = 0
      for change in last_changes:
        count = count - 1

        ci = cis.filter(change=change).first()

        fform = None
        if ci:
          fform = FeedbackForm(selected=ci.improvement_category)
        elif change.user and change.user == request.user and unit.target != '' and change.target != unit.target and change.action == change.ACTION_SUGGESTION:
          fform = FeedbackForm(selected=None)

        change_list.append({ 
          "change": change,
          "improvement": ci,
          "form": fform,
          "count": count
        })

    #ec = None
    #if len(last_changes) > 0:
    #    ec = last_changes[0].errorcategory
    
   
    nearby = Unit.objects.prefetch().filter(
        translation=translation,
        position__gte = offset - settings.NEARBY_MESSAGES,
        position__lte = offset + settings.NEARBY_MESSAGES,
    )
    
    textrepo = TextRepo.objects.filter(
        repo = translation.subproject
    )
    
    return render(
        request,
        'editor.html',
        {
            'is_segment': ('offset' in request.GET or 'checksum' in request.GET or True),
            'this_unit_url': this_unit_url,
            'first_unit_url': base_unit_url + '0',
            'last_unit_url': base_unit_url + str(num_results - 1),
            'next_unit_url': next_unit_url,
            'prev_unit_url': base_unit_url + str(offset - 1),
            'base_unit_url': base_unit_url,
            'object': translation,
            'project': translation.subproject.project,
            'unit': unit,
            'others': others,
            'others_count': others.exclude(target=unit.target).count(),
            'total': translation.unit_set.all().count(),
            'search_url': search_result['url'],
            'search_query': search_result['query'],
            'offset': offset,
            'filter_name': search_result['name'],
            'filter_count': num_results,
            'filter_pos': offset + 1,
            'form': form,
            'antispam': antispam,
            'comment_form': CommentForm(),
            'search_form': search_result['form'],
            'update_lock': translation.lock_user == request.user,
            'secondary': secondary,
            'locked': locked,
            'user_locked': user_locked,
            'project_locked': project_locked,
            'glossary': Dictionary.objects.get_words(unit),
            'addword_form': InlineWordForm(),
            'last_changes': last_changes,
            'change_list': change_list,
            'nearby': nearby,
            'textrepo': textrepo[0] if textrepo.exists() else None,
            'improvementcategory': ic
        }
    )
    
    
    
    
@login_required
def comment(request, pk):
    """Add new comment."""
    unit = get_object_or_404(Unit, pk=pk)
    check_access(request, unit.translation.subproject.project)

    if not can_add_comment(request.user, unit.translation.subproject.project):
        raise PermissionDenied()

    form = CommentForm(request.POST)

    if form.is_valid():
        if form.cleaned_data['scope'] == 'global':
            lang = None
        else:
            lang = unit.translation.language
        Comment.objects.add(
            unit,
            request.user,
            lang,
            form.cleaned_data['comment']
        )
        
        last_changes = unit.change_set.content().order_by('-timestamp')[:1]
        last_changes = Change.objects.filter(
            unit_id = pk
        ).order_by('-timestamp')[:1]
                
        send_notification(unit, request.user)
        
        
        messages.success(request, _('Posted new comment'))
    else:
        messages.error(request, _('Failed to add comment!'))

    return redirect_next(request.POST.get('next'), unit)
