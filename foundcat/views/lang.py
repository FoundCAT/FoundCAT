#from weblate.lang.models import Language
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from weblate.trans.models import Translation

from django.shortcuts import render, redirect
from django.http import Http404
from django.utils.translation import ugettext as _
from django.utils.http import urlencode



from django.shortcuts import render, redirect
from django.http import Http404
from django.utils.translation import ugettext as _
from django.utils.http import urlencode

from weblate.lang.models import Language
from weblate.trans.forms import SiteSearchForm
from weblate.trans.models import Project, Change, SubProject
from weblate.trans.stats import get_per_language_stats
from weblate.trans.util import sort_objects, translation_percent
from weblate.trans.views.helper import get_project
from foundcat.forms import LanguagePairForm

def pairs(request, lang, targetlang):
    try:
        lang1 = Language.objects.get(code=lang)
        lang2 = Language.objects.get(code=targetlang)
    except Language.DoesNotExist:
        raise Http404('No Language matches the given query.')

    projects = Project.objects.all_acl(request.user).filter(
      source_language=lang1
    )
    translations = Translation.objects.prefetch().filter(
      language=lang2,
      subproject__project__in=projects
    )
    
    covered_subprojects = set()
    for translation in translations:
      covered_subprojects.add(translation.subproject.pk)

    other_subprojects = SubProject.objects.prefetch().filter(
      project__in=projects
    ).exclude(
      pk__in=covered_subprojects
    )

    form = LanguagePairForm(initial = {'source': lang1.pk, 'target': lang2.pk })

    langs = Language.objects.all()
    add_url = reverse('repo_add', kwargs = {'project': projects[0].slug})
       
    return render(
        request,
        'pairs.html',
        {
            'form': form,
            'lang1': lang1,
            'lang2': lang2,
            'translations': translations,
            'other_subprojects': other_subprojects,
            'add_url': add_url

        }
    )

def choose_langpair(request):
  if request.method == 'POST':
    form = LanguagePairForm(request.POST)
    if form.is_valid():
      lang = form.cleaned_data['source'].code
      targetlang = form.cleaned_data['target'].code
      return HttpResponseRedirect(reverse('pairs', args=(lang,targetlang)))
