# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.




from __future__ import unicode_literals

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Sum, Count, F
from django.http import HttpResponse
from django.shortcuts import redirect, get_object_or_404
from django.utils import translation
from django.utils.encoding import force_text
from django.utils.html import escape
from django.utils.http import urlencode
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
import django.views.defaults

from weblate.utils import messages
from weblate.trans.models import (
    Project, Translation, Check, ComponentList, Change, Unit,
)
from weblate.requirements import get_versions, get_optional_versions
from weblate.lang.models import Language
from weblate.trans.forms import (
    get_upload_form, SearchForm, SiteSearchForm,
    AutoForm, ReviewForm, get_new_language_form,
    ReportsForm, ReplaceForm,
    ProjectSettingsForm,
)
from weblate.permissions.helpers import (
    can_automatic_translation, can_edit_subproject, can_edit_project,
    can_translate,
)
from weblate.accounts.models import Profile
from weblate.accounts.notifications import notify_new_language
from weblate.trans.stats import get_per_language_stats
from weblate.trans.views.helper import (
    get_project, get_subproject, get_translation,
    try_set_language,
)
from weblate.trans.util import (
    render, sort_objects, sort_unicode, translation_percent,
)
import weblate
from django.conf import settings


def home(request, launch_redirect_url='ok'):

    try:
      if hasattr(request, 'LTI'):
          course_name = request.LTI.get('context_title')
          slug = settings.LTI_COURSE_PROJECT_ALIASES.get(course_name, settings.NON_LTI_DEFAULT_PROJECT)
          if slug:
              obj = Project.objects.get(slug=slug)
      else:
          project = settings.NON_LTI_DEFAULT_PROJECT
          obj = get_project(request, project)
    except Project.DoesNotExist:
      return redirect(reverse('admin:trans_project_changelist'))

    return redirect(reverse('pairs', kwargs={'lang': obj.source_language.code, 'targetlang': 'en_GB'}))


    add_url = reverse('repo_add', kwargs = {'project': obj.slug})
    return render(
        request,
        'dashboard.html',
        {
            'allow_index': True,
            'object': obj,
            'project': obj,
            'add_url': add_url,
        }
    )
    
def foundcat_lti(request, resource_id):
    return redirect('home')
    
