# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.views.decorators.cache import never_cache
from django.contrib.auth.decorators import login_required
from foundcat.forms import SettingsForm
from foundcat.models import ProfileExtension
from weblate.trans.util import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from weblate.utils import messages

@never_cache
@login_required
def settings(request):
  user = request.user
  pe = ProfileExtension.objects.filter(user=user)
  if pe.count() == 0:
    pe = ProfileExtension.objects.create(user=user)
    pe.save()
  else:
    pe = pe[0]

  form = SettingsForm(instance=pe)
  if request.method == "POST":
    form = SettingsForm(request.POST, instance=pe)
    if form.is_valid():
      form.save()
      messages.success(request, 'Settings have been saved.')
      return HttpResponseRedirect(reverse('profile'))


  return render(
      request,
      'accounts/settings.html',
      {
         'form': form
      }
  )
