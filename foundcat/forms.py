# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.urlresolvers import reverse
from django.forms import ModelForm
from foundcat.models import TextRepo, ImprovementCategory, ProfileExtension
from weblate.trans.models.change import Change, Project
from weblate.lang.models import Language
import django.forms as forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit
class SettingsForm(ModelForm):
  class Meta:
    model = ProfileExtension
    exclude = ('user',)

class TextRepoForm(ModelForm):
    
    class Meta:
        model = TextRepo
        fields = (
            'title',
            'source',
            'text',
        )
        
class TextRepoAddForm(ModelForm):
    
    class Meta:
        model = TextRepo
        fields = (
            'title',
            'source',
        )
        
class TextRepoWikiForm(ModelForm):
    
    class Meta:
        model = TextRepo
        fields = (
            'title',
            'text',
        )
            
class FeedbackForm(forms.Form):
    
    def __init__(self, *args, **kwargs):
        selected = kwargs.pop('selected')
        super(FeedbackForm, self).__init__(*args, **kwargs)
        self.fields['improvementcategory'].widget.attrs.update({
            'class': 'form-control'
        })
        if selected:
            self.initial['improvementcategory'] = selected
    
    improvementcategory = forms.ModelChoiceField(label = '', queryset=ImprovementCategory.objects.order_by('title').all())
    
class LanguagePairForm(forms.Form):

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
#        self.helper.form_id = 'id-exampleForm'
#        self.helper.form_class = 'blueForms'
#        self.helper.form_method = 'post'

        self.helper.form_action = reverse('choose_langpair')
        self.helper.add_input(Submit('submit', 'Submit'))
        super(LanguagePairForm, self).__init__(*args, **kwargs)

    source = forms.ModelChoiceField(queryset=Language.objects.filter(
      pk__in=Project.objects.values('source_language__pk').distinct()
    ))
    target = forms.ModelChoiceField(queryset=Language.objects.all())

