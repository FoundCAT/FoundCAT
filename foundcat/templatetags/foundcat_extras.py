# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



from django import template
from django.utils.html import format_html, escape, urlize
from weblate.trans.models.translation import Translation
from weblate.trans.models.change import Change
register = template.Library()

from django.db.models.expressions import RawSQL, F

from django.urls import reverse

from weblate.trans.templatetags.translations import NEWLINES_RE, fmt_whitespace, SPACE_NL
from django.utils.translation import ugettext as _, ungettext, ugettext_lazy

from django.utils.encoding import force_text

from django.utils.safestring import mark_safe

from foundcat.views.edit import FoundcatPluralTextarea

from foundcat.models import TextRepo, ChangeImprovement
from django.core.cache import cache

@register.inclusion_tag('source_text_glossary.html')
def show_source_text_glossary(source_text, glossary, diff=None):
    expr_connector = u':___fc$'
    value = source_text

    raw_value = escape(force_text(source_text))

    new_words = []

    exps = raw_value
    for glossary_word in glossary:
        exps = exps.replace(glossary_word.source, glossary_word.source.replace(u' ', expr_connector))
    
    words = exps.split(u' ')

    used_words = []

    for word in words:
        word = word.replace(expr_connector, ' ')
        replacement = None
        for glossary_word in glossary:
            if word.find(glossary_word.source) != -1:
                if not glossary_word.source in used_words:
                    used_words.append(glossary_word.source)
                    html = format_html(
                      u'<span class="in-glossary" title="{}" data-toggle="tooltip" data-placement="bottom" data-source="{}" data-target="{}">{}</span>',
                      glossary_word.source + u': ' + glossary_word.target,
                      glossary_word.source,
                      glossary_word.target,
                      glossary_word.source
                    )
                else:
                    html = format_html(
                      u'<span data-source="{}" data-target="{}">{}</span>',
                      glossary_word.source,
                      glossary_word.target,
                      glossary_word.source
                    )
                
                replacement = word.replace(glossary_word.source, html, 1)
                break
    
        if replacement: 
            new_words.append(replacement)
        else:
            new_words.append(u'<span>{}</span>'.format(word))
        
    raw_value = u' '.join(new_words)
    raw_value = raw_value.replace('<span></span> ', ' ') # Since double quotes are detected as an empty word, we need to remove the spans around it so they can be detected later
    
    # Normalize newlines
    value = NEWLINES_RE.sub(u'\n', raw_value)

    # Split string
    paras = value.split(u'\n')

    # Format whitespace in each paragraph
    paras = [fmt_whitespace(p) for p in paras]

    newline = SPACE_NL.format(_('New line'))

    # Join paragraphs
    content = mark_safe(newline.join(paras))

    html = content

    return {
        'glossary_html': html,
    }

  
@register.inclusion_tag('recent-user.html')
def recent_project_change(s):
    #actions = set((
    #    Change.ACTION_COMMENT,
    #    Change.ACTION_NEW,
    #    Change.ACTION_CHANGE,
    #    Change.ACTION_NEW_SOURCE,
    #))
    #change = Change.objects.filter(subproject=s, action__in=actions).order_by('-timestamp').first()
    change = s.change_set.order_by('-timestamp').first()

    return {
        'change': change 
    }

@register.inclusion_tag('recent-user.html')
def recent_translation_change(t):
    #actions = set((
    #    Change.ACTION_COMMENT,
    #    Change.ACTION_NEW,
    #    Change.ACTION_CHANGE,
    #    Change.ACTION_NEW_SOURCE,
    #))
    #change = Change.objects.filter(translation=t, action__in=actions).order_by('-timestamp').first()
    change=t.change_set.order_by('-timestamp').first()

    return {
        'change': change
    }


def last_change_is_of(unit, user):
    return False 
    
    # could be inefficient
    changes = Change.objects.filter(unit=unit,action__in=Change.ACTIONS_CONTENT).order_by('-timestamp')[:1]
    if len(changes) == 1 and changes[0].user == user:
        return True
    return False
    
    

register.filter(last_change_is_of)
  
@register.inclusion_tag('duo-link.html')
def foundcat_duo_link(proj):
    if not hasattr(proj, 'slug'):
      return { 'title': proj.name }
    return {
      'title': proj.name,
      'url': reverse('translate', kwargs={'project': proj.project.slug, 'subproject': proj.slug, 'lang': 'en_GB'})
    }
  
@register.filter
def duo(url):
    return url + '#current-segment'
    

    
@register.inclusion_tag('accounts/show_badges.html')
def show_badges(user, classname=""):
    comments = Change.objects.filter(action=Change.ACTION_COMMENT, user=user).count()
    reviews = Change.objects.filter(old=F('target'), user=user).exclude(target='').count()
    new_translations = Change.objects.filter(action=Change.ACTION_NEW, old='', user=user).exclude(target='').count()
    changed_translations = Change.objects.filter(action=Change.ACTION_CHANGE, user=user).exclude(target=F('old')).count()
    text_uploads = TextRepo.objects.filter(owner=user).exclude(repo__isnull=True).count()
    glossary_entries = Change.objects.filter(action=Change.ACTION_DICTIONARY_NEW , user=user).count()
    improvements = ChangeImprovement.objects.filter(change__user=user).count()

    total = comments + reviews + new_translations + changed_translations + text_uploads + glossary_entries + improvements
    
    return {
        'u': user,
        'total': total,
        'comments': comments,
        'reviews': reviews,
        'new_translations': new_translations,
        'changed_translations': changed_translations,
        'text_uploads': text_uploads,
        'glossary_entries': glossary_entries,
        'improvements': improvements,
        'classname': classname
    }
    
  
@register.simple_tag
def same(segment, other_segment, cond):
    return segment.pk == other_segment.pk and cond
  

@register.simple_tag
def show_review_button(unit):
  print unit.suggestions
  if unit.suggestions and unit.suggestions.count() == 1 and unit.suggestions[0].target == unit.target:
    return True
  return False
    
@register.simple_tag
def needs_link_to_top(nearby):
    if nearby[0].position > 1:
        return "Go to first segment"
  
@register.simple_tag
def needs_link_to_bottom(nearby):
    if nearby.reverse()[0].position < nearby[0].translation.total:
        return "Go to last segment (#{})".format(nearby[0].translation.total)
  
  
  
@register.simple_tag
def toolbar(form, unit, profile):
    
    area = FoundcatPluralTextarea()
    area.profile=profile
    toolbar = area.get_toolbar(unit.translation.language, 'testfieldname', unit, 0)
    
    return mark_safe(toolbar)
    
