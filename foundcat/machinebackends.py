import requests
import json
from weblate.trans.machine.base import MachineTranslation

# https://github.com/EmilioK97/pydeepl/blob/master/pydeepl/pydeepl.py

JSONRPC_VERSION = '2.0'
DEEPL_METHOD = 'LMT_handle_jobs'

BASE_URL = 'https://www.deepl.com/jsonrpc'
class DeepLTranslation(MachineTranslation):
    """Translation service using strings already translated in Weblate."""
    name = 'DeepL Translation'
    
    def download_languages(self):
        return ('en', 'de', 'fr', 'es', 'it', 'nl', 'pl')
        
    def download_translations(self, source, language, text, unit, user):
        """Download list of possible translations from a service."""
        
        from_lang=source.upper()
        to_lang=language.upper()
        
        parameters = {
            'jsonrpc': JSONRPC_VERSION,
            'method': DEEPL_METHOD,
            'params': {
                'jobs': [
                    {
                        'kind':'default',
                        'raw_en_sentence': text
                    }
                ],
                'lang': {
                    'user_preferred_langs': [
                        from_lang,
                        to_lang
                    ],
                    'source_lang_user_selected': from_lang,
                    'target_lang': to_lang
                },
            },
        }
        
        response = json.loads(requests.post(BASE_URL, json=parameters).text)
        
        if 'result' in response:
            translations = response['result']['translations']

            if len(translations) > 0 and translations[0]['beams'] is not None and translations[0]['beams'][0]['postprocessed_sentence'] is not None:
                trans = translations[0]['beams'][0]['postprocessed_sentence']
                return [(trans, 100, self.name, text)]
