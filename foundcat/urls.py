# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""foundcat URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin

import foundcat.views.dashboard as dashboard
import foundcat.views.repo as repo
import foundcat.views.edit as edit
import foundcat.views.dictionary as dictionary
import foundcat.views.files as files
import foundcat.views.lang as lang

import weblate.trans.views.js
import weblate.trans.views.lock
import weblate.trans.views.edit
import django.views.i18n

import foundcat.accounts_urls
#import weblate.trans.views.basic
import foundcat.views.basic
import weblate.trans.views.widgets

import django_app_lti.urls

from foundcat.admin import SITE as admin

# URL regexp for language code
LANGUAGE = r'(?P<lang>[^/]+)'

# URL regexp for language code
TARGETLANGUAGE = r'(?P<targetlang>[^/]+)'



# URL regexp for project
PROJECT = r'(?P<project>[^/]+)/'

# URL regexp for subproject
SUBPROJECT = PROJECT + r'(?P<subproject>[^/]+)/'

# URL regexp for translations
TRANSLATION = SUBPROJECT + LANGUAGE + '/'


# URL regexp used as base for widgets
WIDGET = r'(?P<widget>[^/-]+)-(?P<color>[^/-]+)'
# Widget extension match
EXTENSION = r'(?P<extension>(png|svg|bin))'

urlpatterns = [

    url(r'^wl/', include('foundcat.wl_urls')),
    
    url(
        r'^$', 
        dashboard.home, 
        name='home'
    ),

    url(
      r'^pair/' + LANGUAGE + '/' + TARGETLANGUAGE + '$',
      lang.pairs,
      name='pairs'
    ),
    url(
      r'^pair_choose$',
      lang.choose_langpair,
      name='choose_langpair'
    ),

    url(
      r'^foundcat-add-lang/' + SUBPROJECT + '$',
      foundcat.views.basic.new_language,
      name="foundcat-add-lang"
    ),

    url(
        r'^download/translation/' + TRANSLATION + '$',
        files.download_translation,
        name='download_translation_doc',
    ),

    

    # Duolayout
    url(
        r'^translate/' + TRANSLATION + '$',
        edit.translate,
        name='translate',
    ),
    

    # Comments
    url(
        r'^comment-fc/(?P<pk>[0-9]+)/$',
        edit.comment,
        name='comment-fc',
    ),

    # GET (overview)
    url(
        r'^projects/' + PROJECT + 'add/$',
        repo.add,
        name='repo_add',
    ),
    # POST
    url(
        r'^projects/' + PROJECT + 'add_wiki/$',
        repo.add_wiki,
        name='repo_add_wiki',
    ),

    # GET wiki_query=
    url(
        r'^projects/' + PROJECT + 'upload_wiki/$',
        repo.upload_wiki,
        name='repo_upload_wiki',
    ),
    url(
        r'^projects/' + PROJECT + 'edit/' + r'(?P<subproject>[^/]+)/',
        repo.edit,
        name='repo_edit',
    ),
    # POST
    url(
        r'^projects/' + PROJECT + 'upload_text',
        repo.upload,
        name='repo_upload',
    ),
    #Translation pages
    url(
        r'^download/source/' + SUBPROJECT + '$', # By Sebi
        files.download_source,
        name='download_source',
    ),

    
    url(
        r'^js/foundcat-glossary/(?P<unit_id>[0-9]+)/$',
        dictionary.add_dictionary, # By Sebi
        name='foundcat-js-add-glossary',
    ),
    
    url(
        r'^js/foundcat-set-error-category/(?P<change_id>[0-9]+)/$',
        edit.set_error_category, # By Sebi
        name='js-set-error-category',
    ),

    url(r'^admin/', include(admin.urls)),


    url(r'^accounts/', include('foundcat.accounts_urls')),
    

    # Include the lti app's urls
    url(r'^lti/', include(django_app_lti.urls, namespace="lti")),
    url(r'^lti/success/(?P<resource_id>[0-9]+)/', dashboard.foundcat_lti, name='foundcat_lti',)
    
    #url(r'^', include('weblate.urls')),

]
