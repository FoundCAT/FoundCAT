# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.views.decorators.http import require_POST
from django.utils.translation import ugettext as _
from django.contrib.auth import views as auth_views
from django.core.urlresolvers import reverse
from weblate.utils import messages
from django.conf import settings

@require_POST
@login_required
@never_cache
def foundcat_logout(request):
    """Logout handler, just wrapper around standard Django logout."""
    messages.info(request, _('Thanks for using FoundCAT!'))

    return auth_views.logout(
        request,
        next_page=settings.LOGOUT_REDIRECT_URL or reverse('home'),
)
