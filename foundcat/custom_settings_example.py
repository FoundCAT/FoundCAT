# -*- coding: utf-8 -*-
#
# Copyright © FoundCAT <team.foundcat@fh-aachen.de>
#
# This file is part of FoundCAT <https://cat.fh-aachen.de/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

ALLOWED_HOSTS = [
    '' # your IP here
    #'127.0.0.1'
]


# LDAP imports, uncomment if you want to use them
#import ldap
#from django_auth_ldap.config import LDAPSearch

AUTH_LDAP_SERVER_URI = "ldap://example.domain.org/"
AUTH_LDAP_START_TLS = True
AUTH_LDAP_USER_DN_TEMPLATE = "uid=%(user)s,ou=users,dc=example,dc=local"
AUTH_LDAP_BIND_DN = "..."
AUTH_LDAP_BIND_PASSWORD = "..."
AUTH_LDAP_USER_SEARCH = None#LDAPSearch("...",
#    ldap.SCOPE_SUBTREE, "...")
AUTH_LDAP_USER_ATTR_MAP = {"..."}


ADMINS = ""

# E-mail address that error messages come from.
SERVER_EMAIL = 'foundcat@example.org'

# Default email address to use for various automated correspondence from
# the site managers. Used for registration emails.
DEFAULT_FROM_EMAIL = SERVER_EMAIL

EMAIL_HOST_USER = SERVER_EMAIL
EMAIL_HOST_PASSWORD = None
EMAIL_HOST = 'smtp.example.org'
EMAIL_PORT = 587
EMAIL_USE_TLS = True


GITHUB_USERNAME = None


LTI_OAUTH_CREDENTIALS = {
#    "mykey":"mysecret",
#    "myotherkey": "myothersecret",
}


LTI_COURSE_PROJECT_ALIASES = {
    'Name of your LMS course': 'django-project-slug'
}
NON_LTI_DEFAULT_PROJECT = 'django-project-slug'



# Machine translation API keys

# URL of the Apertium APy server
MT_APERTIUM_APY = None

# Microsoft Translator service, register at
# https://datamarket.azure.com/developer/applications/
MT_MICROSOFT_ID = None
MT_MICROSOFT_SECRET = None

# Microsoft Cognitive Services Translator API, register at
# https://portal.azure.com/
MT_MICROSOFT_COGNITIVE_KEY = None

# MyMemory identification email, see
# http://mymemory.translated.net/doc/spec.php
MT_MYMEMORY_EMAIL = None

# Optional MyMemory credentials to access private translation memory
MT_MYMEMORY_USER = None
MT_MYMEMORY_KEY = None

# Google API key for Google Translate API
MT_GOOGLE_KEY = None

# API key for Yandex Translate API
MT_YANDEX_KEY = None

# tmserver URL
MT_TMSERVER = None


SECRET_KEY = None
