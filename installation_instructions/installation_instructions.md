# Install system requirements
apt install python virtualenv pip python-dev 

mkdir foundcat
cd foundcat

# clone foundcat repository into current directory
git clone https://gitlab.com/FoundCAT/foundcat.git .

# Copy sample settings and edit them to your needs
# See Weblate and Django documentation for help on settings.py
cp foundcat/custom_settings_example.py foundcat/custom_settings.py
nano foundcat/custom_settings.py

# Create new virtual environment
virtualenv env

# Activate virtual environment
source env/bin/activate

# If you want to use LDAP for user authentication, open requirements.txt and uncomment ldap specific lines.
# nano requirements.txt

# Install foundcat requirements
pip install -r requirements.txt

# Make and run migrations and load initial data:
./manage.py migrate
./manage.py makemigrations foundcat
./manage.py makemigrations
./manage.py migrate
./manage.py loaddata improvement_categories

# Automatically copy static files of all apps to your data directory
./manage.py collectstatic

# Create admin account:
./manage.py createadmin

# Login as admin, ignore exception (project is missing), go to http://127.0.0.1:8000/admin/ and add project "Technisches English" via the Django settings (this is hardcoded right now and we will change that later)

# Grant read/write permission to executing user, eg. www-data if you use Apache2
chown -R www-data:www-data data/

# Run script to check whether server works
./runserver.sh

# Copy, edit and enable foundcat.apache2.conf from installation_instructions directory

# Done!
